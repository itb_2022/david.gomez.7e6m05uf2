import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MainKtTest{

    @Test
    fun phoneCorrectExample(){
        assertTrue(isValidPhone("679785243"))
    }

    @Test
    fun phoneInvalidLenghtNumber(){
        assertFalse(isValidPhone("6797852445436345345345"))
    }

    @Test
    fun phoneSpecialSymbol(){
        assertFalse(isValidPhone("67978524!"))
    }

    @Test
    fun phoneLetter(){
        assertFalse(isValidPhone("67978524A"))
    }



    @Test
    fun workerDateNotRetired(){
        assertFalse(isRetired("04/06/1988"))
    }

    @Test
    fun workerDateRetired(){
        assertTrue(isRetired("16/05/1942"))
    }

    @Test
    fun workerOneDayToRetire(){
        assertFalse(isRetired("13/12/1957"))
    }

    @Test
    fun workerExactDayToRetire(){
        assertTrue(isRetired("12/12/1957"))
    }



    @Test
    fun dniCorrectExample(){
        assertTrue(isValidDni("33842962A"))
    }

    @Test
    fun dniWithoutLetter(){
        assertFalse(isValidDni("33842962!"))
    }

    @Test
    fun dniIncorrectLenght(){
        assertFalse(isValidDni("33842962334545A"))
    }

    @Test
    fun dniIncorrectFinalLetter(){
        assertFalse(isValidDni("33842962B"))
    }

    @Test
    fun dniNullFinalLetter(){
        assertFalse(isValidDni("33842962 "))
    }



    @Test
    fun passwordCorrectExample(){
        assertTrue(isValidPassword("asdDaDA23!???"))
    }

    @Test
    fun passwordExceptLettersUpperCase(){
        assertFalse(isValidPassword("asdasd123123!!"))
    }

    @Test
    fun passwordExceptLettersLowerCase(){
        assertFalse(isValidPassword("ASDASD123123!!"))
    }

    @Test
    fun passwordExceptDigits(){
        assertFalse(isValidPassword("ASDASDasdasd!!!"))
    }

    @Test
    fun passwordExceptSpecialSymbol(){
        assertFalse(isValidPassword("ASDASDasdasd123123"))
    }

    @Test
    fun passwordIncorrectLenght(){
        assertFalse(isValidPassword("aA1!"))
    }
}